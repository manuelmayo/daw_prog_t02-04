#t02_04.2_figuras

def figurillas():
	print("="*9+" EL FIGURILLAS "+"="*9)
	print("Vamos a dibujar figuras (CTRL-C para terminar)")
	try:
		tipo = input("Escoge el tipo: (1 - Cuadrado, 2 - Triángulo): ")
		if tipo in ["1","2"]:
			altura = input("Cuál es la altura: ")
			if altura.isdecimal():
				relleno = input("Y el relleno: ")
				altura = int(altura)
				if tipo == "1":
					print()
					for i in range(altura):
						print((relleno+" ")*altura)
					print()
				elif tipo == "2":
					print()
					ancho = str(altura*2)
					for i in range(altura):
						print(("{:^"+ancho+"}").format(relleno+" "+(relleno+" ")*i))
					print()
			else:
				print("La altura debe ser un númeroro entero")
				print()
		else:
			print("El tipo no es válido")
			print()
		return True
	except KeyboardInterrupt:
		print()
		print("Ciao!!!!")
		return False

bucle = True
while bucle:
	bucle = figurillas()