#t02_04.4_myma

"""Modulo de funciones matemáticas (myma.py)"""

import math

def es_par(n):
	"""Comprueba si un número es par
	
	>>> es_par(13)
	False
	
	>>> es_par(4)
	True
	
	>>> es_par('par')
	'E'
	
	Args:
		n (int): El número del que deseamos saber si es par.
	Returns:
		(boolean)
	"""
	try:
		if n%2==0:
			return True
		else:
			return False
	except:
		return "E"
		
def ecu_1g(a,b): #a*x+b=0
	"""Resuelve una ecuación de primer grado: a*x+b=0
	
	>>> ecu_1g(2,-4)
	2.0
	
	>>> ecu_1g(0,3)
	'E'
	
	>>> ecu_1g(1,"3")
	'E'
	
	Args:
		a (float): coeficiente a
		b (float): coeficiente b
	Returns:
		(float): El valor de x
	"""
	try:
		x = -b/a
		return x
	except:
		return "E"
		
def ecu_2g(a,b,c): #a*x²+b*x+c=0
	"""Resuelve una ecuación de segundo grado: a*x²+b*x+c=0
	
	>>> ecu_2g(1,-5,6)
	(3.0, 2.0)
	
	>>> ecu_2g(1,-2,1)
	(1.0, 1.0)
	
	>>> ecu_2g(1,1,1)
	'E'
	
	Args:
		a (float): Coeficiente a
		b (float): coeficiente b
		c (float): Coeficiente c
	Returns:
		(tuple [float]): Los valores de x.
	"""
	try:
		discr = b**2-4*a*c
		if discr > 0:
			try:
				x1 = (-b + math.sqrt(discr))/(2*a)
				x2 = (-b - math.sqrt(discr))/(2*a)
				return (x1,x2)
			except ZeroDivisionError:
				#print("ZeroDivisionError: El coeficiente 'a' debe ser distinto de 0")
				return "E"
		elif discr == 0:
			try:
				x1 = -b/(2*a)
				return (x1,x1)
			except ZeroDivisionError:
				#print("ZeroDivisionError: El coeficiente 'a' debe ser distinto de 0")
				return "E"
		elif discr < 0:
			#print("El discriminante 'vb²-4·a·c' es negativo. No tiene soluciones reales.")
			return "E"
	except:
		return "E"
		
def es_primo(n):
	"""Comprueba si un número es primo
	
	>>> es_primo(13)
	True
	
	>>> es_primo(8)
	False
	
	>>> es_primo('8')
	'E'
	
	Args:
		n (int): El número del que deseamos saber si es primo.
	Returns:
		(boolean)
	"""
	try:
		for i in range(2,int(n/2)+1):
			if n%i==0:
				return False
		return True
	except:
		return 'E'
	
def fibo(n):
	"""Devuelve una lista con los números de la serie de Fibonacci hasta 'n'.
	
	>>> fibo(10)
	[1, 1, 2, 3, 5, 8]
	
	>>> fibo(-3)
	'E'
	
	Args:
		n (int): El número máximo que podrán tener los valores de la serie.
	Returns:
		(list [int]): Lista con los números de la serie.
	"""
	try:
		if n > 0:
			x,y = 1,0
			list = []
			while x <= n:
				list.append(x)
				x,y = x+y,x
			return list
		else:
			return 'E'
	except:
		return "E"
		
if __name__ == '__main__':
	import doctest
	doctest.testmod(verbose=True)