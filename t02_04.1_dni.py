#t02_04.1_dni

def calcula_letra_DNI(dni):
	if  ((isinstance(dni, str) and len(dni) == 8 and dni.isdecimal())
		or (isinstance(dni, int) and len(str(dni)) == 8)):
		tabla = "TRWAGMYFPDXBNJZSQVHLCKE"
		intdni = int(dni)
		resto = intdni%23
		letra = tabla[resto]
		return letra
	else:
		return "ERR"
		
print(calcula_letra_DNI("45952559"))
print(calcula_letra_DNI(45952559))
print(calcula_letra_DNI(1))
print(calcula_letra_DNI("patata"))