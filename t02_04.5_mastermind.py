#t02_04.5_mastermind

from random import randint

def mastermind():
	print()
	print("{0:#>5} {1} {0:#>5}".format("#","M A S T E R M I N D"))
	print()
	digitos = input("¿De cuántos dígitos va a ser el número oculto? (min:2 máx:9): ")
	while not (digitos.isnumeric() and int(digitos) >= 2 and int(digitos) <= 9):
		print("El número de digitos no es válido")
		digitos = input("¿De cuántos dígitos va a ser el número oculto? (min:2 máx:9): ")
	print("¡Bien! Ya pensé un número. A ver si lo adivinas...")
	digitos = int(digitos)
	min = 10**(digitos-1)
	max = (10**(digitos))-1
	oculto = str(randint(min,max))
	win=False
	intentos=0
	while not win:
		intentos += 1
		numero = input("¿Cuál podrá ser?: ")
		while not (numero.isnumeric and len(numero)==digitos):
			print("Escribe un número válido")
			numero = input("¿Cuál podrá ser?: ")
		d_bien_colocados = 0
		d_mal_colocados = 0
		if numero == oculto:
				print("¡¡¡Acertaste!!!")
				print("El número era {0} y realizaste {1} intentos".format(oculto,intentos))
				while True:
					print("¿Quieres probar otra vez? (S/N)")
					resp = input()
					if resp.capitalize()=="S":
						return True
					elif resp.capitalize()=="N":
						return False
		else:
			oculto_comp = oculto
			for i in range(digitos):
				if numero[i] == oculto[i]:
					d_bien_colocados += 1
					oculto_comp = oculto_comp[:i]+oculto_comp[(i+1):]
				elif numero[i] in oculto_comp:
					d_mal_colocados += 1
			print("En {0} hai {1} dígitos bien colocados y {2} dígitos mal colocados".format(
					numero,d_bien_colocados,d_mal_colocados))
	
		

bucle = True
while bucle:
	bucle = mastermind()