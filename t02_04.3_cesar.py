#t02_04.3_cesar

def cesar():
	print("{:*>20}".format(""))
	print("*{:^18}*".format("C E S A R"))
	print("{:*>20}".format(""))
	print("* {:<17}*".format("[C]ifrar"))
	print("* {:<17}*".format("[D]escifrar"))
	print("* {:<17}*".format("[S]alir"))
	print("{:*>20}".format(""))
	opc = input("Opción: ")
	while opc.capitalize() not in ["C","D","S"]:
		print("¡Opción incorrecta!")
		opc = input("Opción: ")
	opc = opc.capitalize()
	if opc in ["C","D"]:
		text = ""
		escribiendo = True
		print("Introduce el texto (EOF para terminar - [Ctrl-D/Ctrl-Z]):")
		while escribiendo:
			try:
				text += input()
			except EOFError:
				escribiendo = False
		try:
			clave = int(input("Clave: "))
		except:
			print("La clave tiene que ser un número entero")
			return True
		if opc == "C":
			text_cifrado = cifrar(text,clave)
			print("Criptograma -->")
			print(text_cifrado)
		elif opc == "D":
			mensaje = cifrar(text,-clave)
			print("Mensaje -->")
			print(mensaje)
		print()
		return True
	elif opc == "S":
		print("Bye!")
		return False

def cifrar(text,clave):
	t = text.upper()
	tcifrado = ""
	for c in t:
		if c>="A" and c<="Z":
			unicode = ord(c)
			newunicode = unicode + clave
			while newunicode < ord("A") or newunicode > ord("Z"):
				if newunicode < ord("A"):
					newunicode += 26
				elif newunicode > ord("Z"):
					newunicode -= 26
			car = chr(newunicode)
			tcifrado += car
		else:
			tcifrado += c
	return tcifrado
	

bucle = True
while bucle:
	bucle = cesar()